import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "card",
  templateUrl: "./card.component.html",
  styleUrls: ["./card.component.css"]
})
export class CardComponent implements OnInit {
  constructor() {}
  @Input() src;
  @Input() noImage;
  @Input() boldText;
  @Input() lighterText;
  ngOnInit() {}
}
