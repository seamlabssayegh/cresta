import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "other-day-slots",
  templateUrl: "./other-day-slots.component.html",
  styleUrls: ["./other-day-slots.component.css"]
})
export class OtherDaySlotsComponent implements OnInit {
  constructor() {}
  @Input() day: any;
  @Input() events: any;
  ngOnInit() {}
}
