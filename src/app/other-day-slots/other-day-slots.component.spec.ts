import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherDaySlotsComponent } from './other-day-slots.component';

describe('OtherDaySlotsComponent', () => {
  let component: OtherDaySlotsComponent;
  let fixture: ComponentFixture<OtherDaySlotsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtherDaySlotsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherDaySlotsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
