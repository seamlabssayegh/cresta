import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "timeslot",
  templateUrl: "./timeslot.component.html",
  styleUrls: ["./timeslot.component.css"]
})
export class TimeslotComponent implements OnInit {
  constructor() {}
  @Input() state;
  @Input() range;
  textColor = "booked";
  stateStyle = { color: this.textColor };

  ngOnInit() {
    this.textColor =
      this.state == "booked"
        ? "#81BF41"
        : this.state == "available"
          ? "#3FB3F8"
          : this.state == "not ready" ? "#DC3B3F" : "lightgray";
    this.stateStyle = { color: this.textColor };
  }
}
