import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "signup-input",
  templateUrl: "./signup-input.component.html",
  styleUrls: ["./signup-input.component.css"]
})
export class SignupInputComponent implements OnInit {
  constructor() {}
  @Input() focusImg;
  @Input() blurImg;
  @Input() btnFocusImg;
  @Input() btnBlurImg;
  @Input() placeholder;
  @Input() multiple;
  @Input() type;
  fileName: string;
  file: any;
  files: Array<any>;
  id = Math.floor(99999 * Math.random()) + "";
  srcImg: string;
  btnSrcImg: string;
  bool;
  ngOnInit() {
    this.srcImg = this.blurImg;
    this.btnSrcImg = this.btnBlurImg;
  }
  printFile() {
    let element = document.getElementById(this.id) as HTMLInputElement;
    console.log("Hey");

    if (element.files) {
      var fileName = element.files[0];
      this.fileName = fileName.name;
      this.onFocus();
    }
  }
  focus = false;

  onSwitch(e) {
    if (!this.bool) {
      this.onFocus();
    } else {
      this.onBlur();
    }
  }

  onBlur() {
    this.srcImg = this.blurImg;
    this.btnSrcImg = this.btnBlurImg;
    this.focus = false;
  }

  onFocus() {
    this.srcImg = this.focusImg;
    this.btnSrcImg = this.btnFocusImg;
    this.focus = true;
  }
}
