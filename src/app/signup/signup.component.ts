import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
@Component({
  selector: "app-signup",
  templateUrl: "./signup.component.html",
  styleUrls: ["./signup.component.css"]
})
export class SignupComponent implements OnInit {
  constructor(private router: Router) {}
  ngOnInit() {}
  step = 1;
  next() {
    this.step++;
    if (this.step == 4) {
      this.router.navigate(["/success"]);
    }
  }
}
