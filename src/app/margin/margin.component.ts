import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "margin",
  templateUrl: "./margin.component.html",
  styleUrls: ["./margin.component.css"]
})
export class MarginComponent implements OnInit {
  @Input() value;
  constructor() {}
  styleObj = {};
  ngOnInit() {
    this.styleObj = { "margin-top.px": this.value };
  }
}
