import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { AppComponent } from "./app.component";
import { RoutingModule } from "../routing/routing.module";
import { RequestsModule } from "../requests/requests.module";
import { LandingComponent } from "./landing/landing.component";
import { SignupComponent } from "./signup/signup.component";
import { MarginComponent } from "./margin/margin.component";
import { SignupInputComponent } from "./signup-input/signup-input.component";
import { UiSwitchModule } from "ngx-toggle-switch/src";
import { SuccessComponent } from "./success/success.component";
import { UiSwitchComponent } from "./ui-switch/ui-switch.component";
import { NavbarComponent } from "./navbar/navbar.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { CalendarModule } from "angular-calendar";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { WavesComponent } from "./waves/waves.component";
import { ReserveComponent } from "./reserve/reserve.component";
import { TodaySlotsComponent } from "./today-slots/today-slots.component";
import { OtherDaySlotsComponent } from "./other-day-slots/other-day-slots.component";
import { TimeslotComponent } from "./timeslot/timeslot.component";
import { TodaySlotComponent } from "./today-slot/today-slot.component";
import { ReservationComponent } from "./reservation/reservation.component";
import { CardComponent } from "./card/card.component";
import { GlobalService } from "./global.service";
@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    SignupComponent,
    MarginComponent,
    SignupInputComponent,
    SuccessComponent,
    UiSwitchComponent,
    NavbarComponent,
    DashboardComponent,
    WavesComponent,
    ReserveComponent,
    TodaySlotsComponent,
    OtherDaySlotsComponent,
    TimeslotComponent,
    TodaySlotComponent,
    ReservationComponent,
    CardComponent
  ],
  imports: [
    BrowserModule,
    CalendarModule.forRoot(),
    RoutingModule,
    RequestsModule,
    BrowserAnimationsModule,
    FormsModule
  ],
  providers: [GlobalService],
  bootstrap: [AppComponent]
})
export class AppModule {}
