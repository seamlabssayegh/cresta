import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { HttpInterceptor } from "../../requests/http-interceptor.service";
@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.css"]
})
export class DashboardComponent implements OnInit {
  constructor(private cdr: ChangeDetectorRef, private http: HttpInterceptor) {}

  offerings = {};
  today: Date;
  utilization = {};
  ngOnInit() {
    this.today = new Date();
    this.getOfferings();
  }

  getOfferings() {
    var that = this;
    this.http
      .get("api/space/offerings?space_id=1")
      .then(function(data) {
        var response = JSON.parse(data.text());
        that.offerings = response.success.offerings;
        console.log(that.offerings);
        for (
          let index = 0;
          index < response.success.offerings.length;
          index++
        ) {
          const element = response.success.offerings[index];
          that.offerings[element] = true;
        }
      })
      .catch(function(err) {
        //TODO: swal
        console.log(err);
      });
  }
  show = false;
  dateShow = {};
  view = "calendar";
  type = "group";
  setDayShow(day, val) {
    this.dateShow[day.date] = val;
    return "";
  }

  initDayShow(day, val) {
    if (this.dateShow[day.date] != undefined) return;
    return this.setDayShow(day, val);
  }

  formatDate(date) {
    var d = new Date(date),
      month = "" + (d.getMonth() + 1),
      day = "" + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = "0" + month;
    if (day.length < 2) day = "0" + day;

    return [year, month, day].join("-");
  }

  getUtilizationForDay(day) {
    const date = day.date;
    if (this.utilization[date]) {
      return;
    }
    this.utilization[date] = "Loading...";
    const formattedDate = date.toLocaleDateString();
    var weekday = new Array(7);
    weekday[0] = "Sunday";
    weekday[1] = "Monday";
    weekday[2] = "Tuesday";
    weekday[3] = "Wednesday";
    weekday[4] = "Thursday";
    weekday[5] = "Friday";
    weekday[6] = "Saturday";

    var that = this;
    var url =
      "api/space/day_utilization?space_id=1&offering_id=1&date=" +
      this.formatDate(date) +
      "&day=" +
      weekday[date.getDay()];
    this.http
      .get(url)
      .then(function(data) {
        var response = JSON.parse(data.text());
        const ratio = response.success.reserved_hours_percentage;
        that.utilization[date] = ratio + "%";
      })
      .catch(function(err) {
        //TODO: swal
      });
  }

  hoveringDay(day) {
    return this.dateShow[day.date];
  }
}
