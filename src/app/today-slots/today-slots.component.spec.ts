import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TodaySlotsComponent } from './today-slots.component';

describe('TodaySlotsComponent', () => {
  let component: TodaySlotsComponent;
  let fixture: ComponentFixture<TodaySlotsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TodaySlotsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodaySlotsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
