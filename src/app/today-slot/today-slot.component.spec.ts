import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TodaySlotComponent } from './today-slot.component';

describe('TodaySlotComponent', () => {
  let component: TodaySlotComponent;
  let fixture: ComponentFixture<TodaySlotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TodaySlotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodaySlotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
