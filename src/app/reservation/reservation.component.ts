import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "reservation",
  templateUrl: "./reservation.component.html",
  styleUrls: ["./reservation.component.css"]
})
export class ReservationComponent implements OnInit {
  constructor() {}
  @Input() full;
  @Input() src;
  ngOnInit() {}
}
