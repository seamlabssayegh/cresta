import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";
import { LandingComponent } from "../app/landing/landing.component";
import { SignupComponent } from "../app/signup/signup.component";
import { SuccessComponent } from "../app/success/success.component";
import { DashboardComponent } from "../app/dashboard/dashboard.component";
import { ReserveComponent } from "../app/reserve/reserve.component";

const appRoutes: Routes = [
  {
    path: "",
    redirectTo: "/",
    pathMatch: "full"
  },
  { path: "", component: LandingComponent },
  { path: "signup", component: SignupComponent },
  { path: "reserve", component: ReserveComponent },
  { path: "dashboard", component: DashboardComponent },
  { path: "success", component: SuccessComponent }
  // { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(appRoutes, { useHash: true, enableTracing: false })
  ],
  exports: [RouterModule],
  declarations: []
})
export class RoutingModule {}
