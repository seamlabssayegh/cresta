import { Http, Headers } from "@angular/http";
import { Injectable } from "@angular/core";
import "rxjs/add/operator/toPromise";

@Injectable()
export class HttpInterceptor {
  constructor(private http: Http) {}
  domainName = "http://46.101.232.209/";
  get(url: string) {
    var headers = new Headers();
    headers.append(
      "Content-Type",
      "application/x-www-form-urlencoded; charset=utf-8"
    );

    if (window.localStorage["cresta"])
      headers.append("Authorization", window.localStorage["cresta"]);
    url = this.domainName + url;
    return this.http.get(url, { headers: headers }).toPromise();
  }

  post(url, body) {
    var headers = new Headers();
    url = this.domainName + url;
    headers.append(
      "Content-Type",
      "application/x-www-form-urlencoded; charset=utf-8"
    );

    if (window.localStorage["cresta"])
      headers.append("Authorization", window.localStorage["cresta"]);
    return this.http.post(url, body, { headers: headers }).toPromise();
  }

  delete(url) {
    var headers = new Headers();
    url = this.domainName + url;
    headers.append(
      "Content-Type",
      "application/x-www-form-urlencoded; charset=utf-8"
    );

    if (window.localStorage["cresta"])
      headers.append("Authorization", window.localStorage["cresta"]);
    return this.http.delete(url, { headers: headers }).toPromise();
  }

  put(url, body) {
    var headers = new Headers();
    url = this.domainName + url;
    headers.append(
      "Content-Type",
      "application/x-www-form-urlencoded; charset=utf-8"
    );

    if (window.localStorage["cresta"])
      headers.append("Authorization", window.localStorage["cresta"]);
    return this.http.put(url, { headers: headers }).toPromise();
  }
}
