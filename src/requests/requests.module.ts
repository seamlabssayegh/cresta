import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpInterceptor } from './http-interceptor.service';
import { HttpModule } from '@angular/http';

@NgModule({
  imports: [
    CommonModule,
    HttpModule
  ],
  declarations: [],
  providers: [HttpInterceptor]
})
export class RequestsModule { }
